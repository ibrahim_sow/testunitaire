package com.sow.fr;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;


public class Signal extends JFrame {
	private ButtonGroup radios;
	private JRadioButton nrz;
	private JRadioButton nrzi;
	private JRadioButton manchester;
	private JRadioButton manchesteDifférentiel;
	private JRadioButton miller;
	private TextField textField;
	private Design desin;
	private Container contentPane;
	private String trameBinaire;
	public Signal() {
		super(" Signal");
		unit();
	}
	public void unit() {
		setSize(800, 600);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel contenu=new JPanel();
		contenu.setLayout(new FlowLayout());
		textField=new TextField(30);
		contenu.add(textField);
		nrz=new JRadioButton("NRZ");
		nrzi=new JRadioButton("NRZI");
		nrz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				trameBinaire=textField.getText();
				System.out.println(trameBinaire);  
				desin.setBoutton("nrz");
				repaint();;
			}
		});
		contenu.add(nrz);
		nrzi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				trameBinaire=textField.getText();
				System.out.println(trameBinaire);  
				desin.setBoutton("nrzi");
				repaint();;
			}
		});
		contenu.add(nrzi);
		manchester=new JRadioButton("Manchester");
		manchester.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trameBinaire=textField.getText();
				desin.setBoutton("manchester");
				repaint();
			}
		});
		contenu.add(manchester);
		manchesteDifférentiel=new JRadioButton("Manchester différentiel");
		manchesteDifférentiel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("manchester");
				trameBinaire=textField.getText();
				desin.setBoutton("manchesteDifférentiel");
				repaint();
			}
		});
		contenu.add(manchesteDifférentiel);
		miller=new JRadioButton("Miller");
		miller.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("manchester");
				trameBinaire=textField.getText();
				desin.setBoutton("miller");
				repaint();

			}
		});
		contenu.add(miller);
		radios=new ButtonGroup();
		radios.add(nrz);
		radios.add(nrzi);
		radios.add(manchester);
		radios.add(manchesteDifférentiel);
		radios.add(miller);
		desin=new Design();
		desin.setLayout(new FlowLayout());
		contentPane=getContentPane();
		desin.setBorder(BorderFactory.createTitledBorder("DESIN"));
		setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		contentPane.add(contenu);
		contentPane.add(desin);
	}
	
	public class Design extends JPanel {
		String boutton ;
		int pasTransionPremier = 40;
		int pos1 = 85;
		int posNull = 145;
		int pos2 = 205;
		char[] courant = new char[1];
		char bitCourant;
		public Design() {
		}
		@Override
		protected void paintComponent(Graphics graphe) {
			super.paintComponent(graphe);
			if(boutton=="nrz") {
				indicateurTention(graphe);
				for (int i = 0; i < trameBinaire.length(); i++) {
					ecritureBit(graphe, i);
					graphe.setColor(Color.RED);
					signalNRZ(graphe, i);
					graphe.setColor(Color.BLACK);
					if (i>0) {
						transmisionNRZEtNRZI(graphe, i);
					}
				}
			}
			if(boutton=="nrzi") {
				indicateurTention(graphe);
				for (int i = 0; i < trameBinaire.length(); i++) {
					ecritureBit(graphe, i);
					graphe.setColor(Color.RED);
					signalNRZI(graphe, i);
					graphe.setColor(Color.BLACK);
					if (i>0) {
						transmisionNRZEtNRZI(graphe, i);
					}
				}
			}
			if (boutton=="manchester") {
				signalMa(graphe);
			}
			if (boutton=="manchesteDifférentiel") {
				signalMachesterDifferetiel(graphe);
			}
			if (boutton=="miller") {
				signalMiller(graphe);
			}
		}
		
		public void signalNRZI(Graphics graphe,int i) {
			if (trameBinaire.charAt(i) == '1') {
				graphe.drawLine(i * pasTransionPremier, pos2, (i * pasTransionPremier) + pasTransionPremier, pos2);

			} else if (trameBinaire.charAt(i) == '0') {
				// 0 est codé  par un signal de n volts
				graphe.drawLine(i * pasTransionPremier, pos1, (i * pasTransionPremier) + pasTransionPremier, pos1);
			}
		}
		
		public void transmisionNRZEtNRZI(Graphics graphe,int i) {
			// Dessin de la transition
			if (i > 0) {
				if (trameBinaire.charAt(i) != trameBinaire.charAt(i - 1))
					// On dessine la transition ssi le bit courant est différent du précédent
					graphe.setColor(Color.RED);
				else
					// L'absence de transition est réprésentée par une transition en blanc
					graphe.setColor(Color.WHITE);
				graphe.drawLine(i * pasTransionPremier, pos1, i * pasTransionPremier, pos2);
			}

		}
		
		public String getBoutton() {
			return boutton;
		}
		
		public  void ecritureBit(Graphics graphe,int  i) {
			// Ecriture en dessous du signal, des bits correspondants
			courant[0] = trameBinaire.charAt(i);
			graphe.setColor(Color.BLACK);
			graphe.drawChars(courant, 0, 1, (i * pasTransionPremier) + 15, pos2 + 25);
		}
		
		public void setBoutton(String b) {
			boutton=b;
		}
		
		public void indicateurTention(Graphics graphe) {
			graphe.drawString("nV", trameBinaire.length(), pos1 - 5);
			graphe.drawString("0V", trameBinaire.length(), posNull);
			graphe.drawString("-nV", trameBinaire.length(), pos2 - 5);
		}
		
		public void signalNRZ(Graphics graphe,int i) {
			if (trameBinaire.charAt(i) == '0') {
				// 0 est codé  par un signal de -n volts
				graphe.drawLine(i * pasTransionPremier, pos2, (i * pasTransionPremier) + pasTransionPremier, pos2);
			} else if (trameBinaire.charAt(i) == '1') {
				// 1 est codé  par un signal de n volts
				graphe.drawLine(i * pasTransionPremier, pos1, (i * pasTransionPremier) + pasTransionPremier, pos1);
			}
		}
		
		public void signalMa(Graphics graphe) {
			pasTransionPremier = 30;
			pos1 = 85;
			posNull = 158;
			pos2 = 230;
			// dessin des indicateurs de tensions
			graphe.drawString("nV", trameBinaire.length(), pos1 - 5);
			graphe.drawString("0V", trameBinaire.length(), posNull);
			graphe.drawString("-nV", trameBinaire.length(), pos2 - 5);
			// dessin du signal correspondant à chaque bit
			for (int i = 0, absCourant = 0; i < trameBinaire.length(); i++) {
				graphe.setColor(Color.RED);
				absCourant = i * 2 * pasTransionPremier;

				if (trameBinaire.charAt(i) == '0') {
					// 0 est codé par une transition de -n à n
					graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
					graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
					graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + 2 * pasTransionPremier, pos1);
					if (i > 0 && trameBinaire.charAt(i - 1) == '0') {
						graphe.drawLine(absCourant, pos1, absCourant, pos2);
					}
				} else if (trameBinaire.charAt(i) == '1') {
					// 1 est codé par une transition de n à -n
					graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
					graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
					graphe.drawLine(absCourant + 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
					if (i > 0 && trameBinaire.charAt(i - 1) == '1') {
						graphe.drawLine(absCourant, pos1, absCourant, pos2);
					}
				}
				// Ecriture en dessous du signal, des bits correspondants
				courant[0] = trameBinaire.charAt(i);
				graphe.setColor(Color.BLACK);
				graphe.drawChars(courant, 0, 1, absCourant + 20, pos2 + 15);
			}
		}
		
		public void signalMachesterDifferetiel(Graphics graphe) {
			pasTransionPremier = 30;
			pos1 = 85;
			posNull = 158;
			pos2 = 230;
			indicateurTention(graphe);
			bitCourant = trameBinaire.charAt(0);
			graphe.setColor(Color.RED);
			boolean PasEuDeTransitionEnDebutHorloge;
			int bit_precedent;
			// Dessin du signal correspondant au 1er bit
			if (bitCourant == '1') {
				// Le bit 1 est codé par une absence de transition en début d’horloge
				graphe.drawLine(0, pos2, pasTransionPremier, pos2);
				graphe.drawLine(pasTransionPremier, pos1, pasTransionPremier, pos2);
				graphe.drawLine(pasTransionPremier, pos1, 2 * pasTransionPremier, pos1);
				PasEuDeTransitionEnDebutHorloge = true;
			} else {
				// le bit 0 est codé par une transition en début d’horloge
				graphe.drawLine(0, pos1, pasTransionPremier, pos1);
				graphe.drawLine(pasTransionPremier, pos1, pasTransionPremier, pos2);
				graphe.drawLine(2 * pasTransionPremier, pos2, pasTransionPremier, pos2);
				PasEuDeTransitionEnDebutHorloge = false;
			}
			// Ecriture en dessous du signal, du premier bit correspondant
			courant[0] = bitCourant;
			graphe.setColor(Color.BLACK);
			graphe.drawChars(courant, 0, 1, 20, pos2 + 20);
			// Dessin du signal correspondant aux bits restants
			for (int i = 1, absCourant = 1; i < trameBinaire.length(); i++) {
				bit_precedent = bitCourant;
				bitCourant = trameBinaire.charAt(i);
				absCourant = i * 2 * pasTransionPremier;
				graphe.setColor(Color.RED);
				if (bit_precedent == bitCourant) {
					if (bitCourant == '1') {
						// Le bit 1 est codé par une absence de transition en début d’horloge
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						} else {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + 2 * pasTransionPremier, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						}
					} else { // Si le bit courant = 0

						// il est codé par une transition en début d’horloge
						graphe.drawLine(absCourant, pos1, absCourant, pos2);
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + 2 * pasTransionPremier, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						} else {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						}
					}
					
				} else {  // Si le bit courant est différent du précedent

					if (bitCourant == '0') {
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant, pos2, absCourant, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + 2 * pasTransionPremier, pos1);
							PasEuDeTransitionEnDebutHorloge = true;

						} else { // bit_courant == '1'
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant, pos1, absCourant, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						}
						
					} else {  // Si le bit courant est différent du précedent et est = 1
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						} else {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + 2 * pasTransionPremier, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						}
					}
				}
				
				// Ecriture en dessous du signal, des bits correspondants au reste du signal
				courant[0] = trameBinaire.charAt(i);
				graphe.setColor(Color.BLACK);
				graphe.drawChars(courant, 0, 1, absCourant + 20, pos2 + 20);
			}
		}
		
		public void signalMiller(Graphics graphe) {
			pasTransionPremier = 30;
			pos1 = 85;
			posNull = 158;
			pos2 = 230;
			indicateurTention(graphe);
			bitCourant = trameBinaire.charAt(0);
			boolean PasEuDeTransitionEnDebutHorloge;
			int bit_precedent;
			// Dessin du signal correspondant au 1er bit
			graphe.setColor(Color.RED);
			if (bitCourant == '1') {
				// Le bit 1 est codé par une transition en milieu de temps horloge
				graphe.drawLine(0, pos2, pasTransionPremier, pos2);
				graphe.drawLine(pasTransionPremier, pos1, pasTransionPremier, pos2);
				graphe.drawLine(pasTransionPremier, pos1, 2 * pasTransionPremier, pos1);
				PasEuDeTransitionEnDebutHorloge = true;
			} else {
				// et le bit 0 est codé par une absence de transition.
				graphe.drawLine(0, pos2, pasTransionPremier, pos2);
				graphe.drawLine(2 * pasTransionPremier, pos2, pasTransionPremier, pos2);
				PasEuDeTransitionEnDebutHorloge = false;
			}
			
			// Ecriture en dessous du signal, du premier bit correspondant
			courant[0] = bitCourant;
			graphe.setColor(Color.BLACK);
			graphe.drawChars(courant, 0, 1, 20, pos2 + 20);
			// Dessin du signal correspondant aux bits restants
			for (int i = 1, absCourant = 1; i < trameBinaire.length(); i++) {
				absCourant = i * 2 * pasTransionPremier;
				bit_precedent = bitCourant;
				bitCourant = trameBinaire.charAt(i);
				graphe.setColor(Color.RED);
				if (bit_precedent == bitCourant) {
					if (bitCourant == '1') {
						// Le bit 1 est codé par une transition en milieu de temps horloge
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier * 2, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						} else {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier * 2, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						}
					} else {
						// Et le bit 0 est codé par une absence de transition.
						// Si un bit 0 est suivi d’un autre 0, une transition est rajoutée en début du tmps d'horloge.
						// g.drawLine(absCourant, nvPos, absCourant, moinsNvPos);
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier * 2, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						} else {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier * 2, pos2, absCourant + pasTransionPremier, pos2);

							PasEuDeTransitionEnDebutHorloge = false;
						}
					}
				} else { // Si bit_precedent != bit_courant

					if (bitCourant == '0') { // Donc bit_precedent = 1
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier * 2, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						} else {
							graphe.drawLine(absCourant, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier * 2, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						}
					} else {  // Si bit_courant == '1'
						if (PasEuDeTransitionEnDebutHorloge) {
							graphe.drawLine(absCourant, pos1, absCourant + pasTransionPremier, pos1);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier * 2, pos2, absCourant + pasTransionPremier, pos2);
							PasEuDeTransitionEnDebutHorloge = false;
						} else {
							graphe.drawLine(i * 2 * pasTransionPremier, pos2, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier, pos2);
							graphe.drawLine(absCourant + pasTransionPremier, pos1, absCourant + pasTransionPremier * 2, pos1);
							PasEuDeTransitionEnDebutHorloge = true;
						}
					}
				}
				// Ecriture en dessous du signal, des bits correspondants au reste du signal
				courant[0] = trameBinaire.charAt(i);
				graphe.setColor(Color.BLACK);
				graphe.drawChars(courant, 0, 1, (i * pasTransionPremier * 2) + 20, pos2 + 20);
			}
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(Signal::new);
		System.out.println("Une modification prise en compte");
		
                System.out.println("Une modification a anuller");
	}



}
